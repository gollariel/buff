package utils

import (
	"encoding/json"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestPassword(t *testing.T) {
	p := Password("secret")
	testutil.TestingAssertEqual(t, p.String(), "********")
	b, err := json.Marshal(map[string]interface{}{
		"password": p,
	})
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, string(b), `{"password":"********"}`)
	testutil.TestingAssertEqual(t, p.Value(), "secret")
}

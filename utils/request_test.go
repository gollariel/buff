package utils

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestNewRequestJSON(t *testing.T) {
	b := bytes.NewBuffer([]byte(`{"test": "example"}`))
	r, err := NewRequestJSON(httptest.NewRequest(http.MethodGet, "/", b))
	if err != nil {
		t.Fatal(err)
	}
	testutil.TestingAssertEqual(t, r.Get("test"), "example")
}

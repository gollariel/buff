package utils

import (
	"os"

	"github.com/google/gops/agent"
)

// EnvGopsAddr name of environment variable for gops addr
const EnvGopsAddr = "GOPS_ADDR"

// RunGopsInstance run gops instance if GOPS_ADDR present
func RunGopsInstance() error {
	gopsAddr := os.Getenv(EnvGopsAddr)
	if gopsAddr == "" {
		gopsAddr = ":6060"
	}
	if err := agent.Listen(agent.Options{Addr: gopsAddr}); err != nil {
		return err
	}
	return nil
}

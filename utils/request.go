package utils

import (
	"encoding/json"
	"net/http"
)

// RequestJSON default response
type RequestJSON struct {
	data map[string]interface{}
}

// NewRequestJSON return new default request
func NewRequestJSON(r *http.Request) (*RequestJSON, error) {
	var requestData map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		return nil, err
	}
	defer func() {
		if closeErr := r.Body.Close(); closeErr != nil {
			err = closeErr
		}
	}()

	return &RequestJSON{
		data: requestData,
	}, nil
}

// Get return parameter
func (r *RequestJSON) Get(key string) interface{} {
	if v, ok := r.data[key]; ok {
		return v
	}
	return nil
}

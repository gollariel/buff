package utils

import (
	"context"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestNewContextKey(t *testing.T) {
	c := NewContextKey("test")
	ctx := context.WithValue(context.Background(), c, 1)
	testutil.TestingAssertEqual(t, ctx.Value(c), 1)
}

package utils

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"
)

// ResponseJSON default response
type ResponseJSON struct {
	Data       map[string]interface{} `json:"data"`
	ServerInfo map[string]interface{} `json:"serverInfo"`
	status     int
	header     map[string]string
}

// NewResponseJSON return new json response
func NewResponseJSON() *ResponseJSON {
	return &ResponseJSON{
		Data: map[string]interface{}{},
		ServerInfo: map[string]interface{}{
			"timestamp": time.Now().Unix(),
		},
		status: http.StatusOK,
		header: map[string]string{},
	}
}

// Set set Status and prepared Data
func (r *ResponseJSON) Set(data map[string]interface{}, status int) *ResponseJSON {
	r.Data = data
	r.status = status
	return r
}

// Add add data into response
func (r *ResponseJSON) Add(key string, value interface{}) *ResponseJSON {
	r.Data[key] = value
	return r
}

// Header add header
func (r *ResponseJSON) Header(key string, value string) *ResponseJSON {
	r.header[key] = value
	return r
}

// SetStatus set status
func (r *ResponseJSON) SetStatus(status int) *ResponseJSON {
	r.status = status
	return r
}

// Debug add Data to serverInfor
func (r *ResponseJSON) Debug(debug map[string]interface{}) *ResponseJSON {
	for k, v := range debug {
		r.ServerInfo[k] = v
	}
	return r
}

// Send send json response
func (r *ResponseJSON) Send(w http.ResponseWriter) {
	data, err := json.Marshal(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	for k, v := range r.header {
		w.Header().Set(k, v)
	}
	w.WriteHeader(r.status)
	if _, err := w.Write(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Send send response
func Send(w http.ResponseWriter, contentType string, status int, data []byte, header map[string]string) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", contentType)
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	for k, v := range header {
		w.Header().Set(k, v)
	}
	w.WriteHeader(status)
	if _, err := w.Write(data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

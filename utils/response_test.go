package utils

import (
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestResponseJSON(t *testing.T) {
	r := NewResponseJSON()
	r.Add("test", 2)
	r.Add("test2", 3)
	r.Header("Content-Type-2", "application/json")
	r.Debug(map[string]interface{}{
		"timestamp": 1564525687,
	})

	rec := httptest.NewRecorder()
	r.Send(rec)

	testutil.TestingAssertJSON(t, strings.Trim(rec.Body.String(), "\n"), `{"data":{"test":2,"test2":3},"serverInfo":{"timestamp":1564525687}}`)
}

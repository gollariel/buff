package user

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/sdb/models"
	"bitbucket.org/gollariel/buff/server"
	"bitbucket.org/gollariel/buff/server/middlewares"
	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestUser(t *testing.T) {
	mr := server.NewMiddleware(middlewares.Recover)
	um := server.NewMiddleware(middlewares.Recover, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxService, models.BaseServiceName))
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxScope, models.ScopeUser))
			next.ServeHTTP(w, r)
		})
	}, middlewares.JWTAuthenticator)

	err := models.DeleteUser("test@test.com")
	if err != nil {
		fastlog.Debug("Error removing test user", "err", err)
	}

	reqCreate, err := http.NewRequest(http.MethodPost, "/user", bytes.NewBufferString(`{
	"email": "test@test.com",
	"first_name": "test",
	"last_name": "test", 
	"password": "123321",
	"birthday": "1992-01-23T00:00:00Z"
}`))
	testutil.TestingAssertEqual(t, err, nil)
	rr := httptest.NewRecorder()
	mr.Then(http.HandlerFunc((&Handler{}).Create)).ServeHTTP(rr, reqCreate)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusAccepted)

	reqAuth, err := http.NewRequest(http.MethodPost, "/user", bytes.NewBufferString(`{
	"email": "test@test.com",
	"password": "123321"
}`))
	testutil.TestingAssertEqual(t, err, nil)
	rr = httptest.NewRecorder()
	mr.Then(http.HandlerFunc((&Handler{}).Auth)).ServeHTTP(rr, reqAuth)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusOK)
	bearerToken := rr.Header().Get("Authorization")

	reqGet, err := http.NewRequest(http.MethodGet, "/user", nil)
	if err != nil {
		t.Fatal(err)
	}
	reqGet.Header.Add("Authorization", bearerToken)
	rr = httptest.NewRecorder()
	um.Then(http.HandlerFunc((&Handler{}).Get)).ServeHTTP(rr, reqGet)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusOK)

	var requestData struct {
		Data struct {
			User models.User `json:"user"`
		} `json:"data"`
	}
	err = json.Unmarshal(rr.Body.Bytes(), &requestData)
	user := requestData.Data.User
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, user.Email, "test@test.com")
	testutil.TestingAssertEqual(t, user.FirstName, "test")
	testutil.TestingAssertEqual(t, user.LastName, "test")

	reqGet, err = http.NewRequest(http.MethodGet, "/user", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()
	um.Then(http.HandlerFunc((&Handler{}).Get)).ServeHTTP(rr, reqGet)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusForbidden)

	reqUpdate, err := http.NewRequest(http.MethodPut, "/user", bytes.NewBufferString(`{
	"first_name": "test123",
	"last_name": "test123"
}`))
	if err != nil {
		t.Fatal(err)
	}
	reqUpdate.Header.Add("Authorization", bearerToken)
	rr = httptest.NewRecorder()
	um.Then(http.HandlerFunc((&Handler{}).Update)).ServeHTTP(rr, reqUpdate)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusAccepted)

	reqGet, err = http.NewRequest(http.MethodGet, "/user", nil)
	if err != nil {
		t.Fatal(err)
	}
	reqGet.Header.Add("Authorization", bearerToken)
	rr = httptest.NewRecorder()
	um.Then(http.HandlerFunc((&Handler{}).Get)).ServeHTTP(rr, reqGet)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusOK)
	err = json.Unmarshal(rr.Body.Bytes(), &requestData)
	user = requestData.Data.User
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, user.Email, "test@test.com")
	testutil.TestingAssertEqual(t, user.FirstName, "test123")
	testutil.TestingAssertEqual(t, user.LastName, "test123")

	reqDelete, err := http.NewRequest(http.MethodDelete, "/user", nil)
	testutil.TestingAssertEqual(t, err, nil)
	reqDelete.Header.Add("Authorization", bearerToken)
	rr = httptest.NewRecorder()
	um.Then(http.HandlerFunc((&Handler{}).Delete)).ServeHTTP(rr, reqDelete)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusAccepted)
}

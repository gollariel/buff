package user

import (
	"net/http"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/requests"
	"bitbucket.org/gollariel/buff/sdb/models"
	"bitbucket.org/gollariel/buff/utils"
)

// Handler helper for handler user related requests
type Handler struct {
}

// GetCurrentUserEmail return email of current user
func (h *Handler) GetCurrentUserEmail(r *http.Request) string {
	emailRaw := r.Context().Value(jwt.CtxEmail)
	if emailRaw == nil {
		return ""
	}
	return emailRaw.(string)
}

// NewHandler return new handler
func NewHandler() *Handler {
	return &Handler{}
}

// Create create user
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var createUserRequest requests.UserRequest
	err := requests.DecodeRequest(r, &createUserRequest)
	if err != nil {
		fastlog.Error("Error decoding request", "err", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	scopes := []string{models.ScopeUser, models.ScopeStream}
	err = models.CreateUser(createUserRequest.Email, createUserRequest.Password, createUserRequest.FirstName, createUserRequest.LastName, createUserRequest.Birthday, scopes)
	if err != nil {
		fastlog.Error("Error creating user", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.SetStatus(http.StatusAccepted)
	resp.Send(w)
}

// Update update user
func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	email := h.GetCurrentUserEmail(r)
	values, err := requests.DecodeMapRequest(r, map[string]string{
		"first_name": "min=3,max=200",
		"last_name":  "min=3,max=200",
		"birthday":   "",
	})
	if err != nil {
		fastlog.Error("Error decoding request", "err", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = models.UpdateUser(email, values)
	if err != nil {
		fastlog.Error("Error updating user", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.SetStatus(http.StatusAccepted)
	resp.Send(w)
}

// Get get user
func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	email := h.GetCurrentUserEmail(r)
	user, err := models.GetUser(email)
	if err != nil {
		fastlog.Error("Error getting user", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.SetStatus(http.StatusOK)
	resp.Add("user", user)
	resp.Send(w)
}

// Delete delete user
func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	email := h.GetCurrentUserEmail(r)
	err := models.DeleteUser(email)
	if err != nil {
		fastlog.Error("Error deleting user", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.SetStatus(http.StatusAccepted)
	resp.Send(w)
}

// Auth authorize user
func (h *Handler) Auth(w http.ResponseWriter, r *http.Request) {
	var authRequest requests.AuthRequest
	err := requests.DecodeRequest(r, &authRequest)
	if err != nil {
		fastlog.Error("Error decoding request", "err", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	token, err := models.AuthUser(authRequest.Email, authRequest.Password)
	if err != nil {
		fastlog.Error("Error auth user", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.Header("Authorization", utils.Concat("Bearer ", token))
	resp.SetStatus(http.StatusOK)
	resp.Send(w)
}

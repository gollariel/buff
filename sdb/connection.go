package sdb

import (
	"sync"

	"bitbucket.org/gollariel/buff/fastlog"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // nolint
)

var (
	connection *Client
	mu         sync.RWMutex
)

// Set set global connection
func Set(r *Client) {
	mu.Lock()
	connection = r
	mu.Unlock()
}

// Get return sql DB connection
func Get() (*Client, error) {
	mu.RLock()
	if connection == nil {
		mu.RUnlock()
		return nil, ErrConnectionIsNotSet
	}
	mu.RUnlock()
	return connection, nil
}

// Default return default client
func Default() (*Client, error) {
	c, err := Get()
	if err != nil {
		config, err := GetConnectionConfigFromEnv()
		if err != nil {
			fastlog.Error("Error getting sql config", "err", err)
			return nil, err
		}
		c, err = NewClient(config)
		if err != nil {
			fastlog.Error("Error getting sql client", "err", err)
			return nil, err
		}
		Set(c)
	}
	return c, nil
}

// Client client for sql DB
type Client struct {
	DB *sqlx.DB
}

// NewClient return client from config
func NewClient(c *ConnectionConfig) (*Client, error) {
	db, err := sqlx.Connect(c.DriverName, c.SourceName)
	if err != nil {
		return nil, err
	}
	return &Client{
		DB: db,
	}, nil
}

package sdb

import "errors"

// All kind of errors for connection
var (
	ErrConnectionIsNotSet = errors.New("connection is not set")
)

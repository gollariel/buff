package sdb

import (
	"github.com/kelseyhightower/envconfig"
)

// EnvPrefix environment prefix for sql config
const EnvPrefix = "SQL"

// ConnectionConfig contains required data for sql DB
type ConnectionConfig struct {
	DriverName string `required:"true" split_words:"true"`
	SourceName string `required:"true" split_words:"true"`
}

// NewDBConfig return new sql config
func NewDBConfig(driverName, sourceName string) *ConnectionConfig {
	return &ConnectionConfig{
		DriverName: driverName,
		SourceName: sourceName,
	}
}

// GetConnectionConfigFromEnv return sql configs bases on environment variables
func GetConnectionConfigFromEnv() (*ConnectionConfig, error) {
	c := new(ConnectionConfig)
	err := envconfig.Process(EnvPrefix, c)
	return c, err
}

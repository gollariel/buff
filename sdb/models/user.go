package models

import (
	"time"

	"bitbucket.org/gollariel/buff/datatypes"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/sdb"
	"bitbucket.org/gollariel/buff/utils"
	"github.com/Masterminds/squirrel"
	"github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
)

// BaseServiceName base auth service
const BaseServiceName = "poll"

// All auth scopes
const (
	ScopeStream = "stream"
	ScopeUser   = "user"
)

// User describe user model
type User struct {
	Email     string         `db:"email"`
	Password  utils.Password `db:"password"`
	FirstName string         `db:"first_name"`
	LastName  string         `db:"last_name"`
	Birthday  time.Time      `db:"birthday"`
	Scopes    pq.StringArray `db:"scopes"`
}

// GetToken return auth token
func (u User) GetToken() (string, error) {
	return jwt.Create(u.Email, BaseServiceName, datatypes.NewDataSetStrings(ScopeStream, ScopeUser), time.Now().Add(time.Hour))
}

// ValidateAccess validate access
func (u User) ValidateAccess(claim jwt.Claim) bool {
	for _, s := range u.Scopes {
		if !claim.Scopes.Contains(s) {
			return false
		}
	}
	return true
}

// Answers describe answers
type Answers struct {
	ID     int    `db:"id"`
	Email  string `db:"email"`
	PollID int    `db:"poll_id"`
	Answer int    `db:"answer_id"`
}

// NewAnswer chose new answer
func NewAnswer(pollID int, answerID int, email string) error {
	sql, args, err := squirrel.Insert(TableAnswers).SetMap(map[string]interface{}{
		"email":     email,
		"poll_id":   pollID,
		"answer_id": answerID,
	}).ToSql()
	if err != nil {
		return err
	}
	c, err := sdb.Default()
	if err != nil {
		return err
	}

	query := c.DB.Rebind(sql)
	_, err = c.DB.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

// GetUser return user by email
func GetUser(email string) (User, error) {
	sql, args, err := squirrel.Select("u.email, u.password, u.first_name, u.last_name, u.birthday, u.scopes").
		From(utils.Concat(TableUsers, " u")).Where("u.email = ?", email).ToSql()
	if err != nil {
		return User{}, err
	}

	c, err := sdb.Default()
	if err != nil {
		return User{}, err
	}

	var users []User
	query := c.DB.Rebind(sql)
	err = c.DB.Select(&users, query, args...)
	if err != nil {
		return User{}, err
	}
	if len(users) == 0 {
		return User{}, ErrNotFoundUser
	}
	return users[0], nil
}

// AuthUser auth user and return token
func AuthUser(email string, password utils.Password) (string, error) {
	u, err := GetUser(email)
	if err != nil {
		return "", err
	}

	if err = bcrypt.CompareHashAndPassword([]byte(u.Password.Value()), []byte(password.Value())); err != nil {
		return "", err
	}
	return u.GetToken()
}

// CreateUser create new user
func CreateUser(email string, password utils.Password, firstName string, lastName string, birthday time.Time, scopes []string) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(password.Value()), 8)
	if err != nil {
		return err
	}
	sql, args, err := squirrel.Insert(TableUsers).SetMap(map[string]interface{}{
		"email":      email,
		"first_name": firstName,
		"last_name":  lastName,
		"birthday":   birthday,
		"scopes":     pq.StringArray(scopes),
		"password":   utils.ByteSliceToString(hash),
	}).ToSql()

	if err != nil {
		return err
	}
	c, err := sdb.Default()
	if err != nil {
		return err
	}

	query := c.DB.Rebind(sql)
	_, err = c.DB.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

// DeleteUser delete user
func DeleteUser(email string) error {
	sql, args, err := squirrel.Delete(utils.Concat(TableUsers, " u")).Where("u.email = ?", email).ToSql()
	if err != nil {
		return err
	}
	c, err := sdb.Default()
	if err != nil {
		return err
	}

	query := c.DB.Rebind(sql)
	_, err = c.DB.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

// UpdateUser update user
func UpdateUser(email string, values map[string]interface{}) error {
	sql, args, err := squirrel.Update(utils.Concat(TableUsers, " u")).SetMap(values).Where("u.email = ?", email).ToSql()
	if err != nil {
		return err
	}
	c, err := sdb.Default()
	if err != nil {
		return err
	}

	query := c.DB.Rebind(sql)
	_, err = c.DB.Exec(query, args...)
	if err != nil {
		return err
	}
	return nil
}

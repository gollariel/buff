package models

import (
	"strings"
	"testing"
	"time"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestTable(t *testing.T) {
	err := DeleteUser("gollariel@gmail.com")
	if err != nil {
		fastlog.Debug("Error removing test user", "err", err)
	}
	err = TruncateAnswers()
	if err != nil {
		fastlog.Debug("Error truncate users", "err", err)
	}

	err = CreateUser("gollariel@gmail.com", "test", "Maxim", "Tkach", time.Now(), []string{ScopeStream, ScopeUser})
	testutil.TestingAssertEqual(t, err, nil)

	token, err := AuthUser("gollariel@gmail.com", "test")
	testutil.TestingAssertEqual(t, err, nil)

	_, err = jwt.Validate(token, BaseServiceName, "123")
	testutil.TestingAssertEqual(t, err, jwt.ErrUserNotInScope)
	_, err = jwt.Validate(token, "123", ScopeUser)
	testutil.TestingAssertEqual(t, err, jwt.ErrUserWrongService)
	c, err := jwt.Validate(token, BaseServiceName, ScopeUser)
	testutil.TestingAssertEqual(t, err, nil)

	u, err := GetUser("gollariel@gmail.com")
	testutil.TestingAssertEqual(t, err, nil)
	if !u.ValidateAccess(c) {
		t.Fatal("Error validating access")
	}

	streams, err := GetStreams(10, 0)
	testutil.TestingAssertEqual(t, err, nil)
	count, err := CountStreams()
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, count, uint64(2))
	for _, s := range streams {
		var polls []Poll
		err := s.Polls.Unmarshal(&polls)
		testutil.TestingAssertEqual(t, err, nil)
		for _, poll := range polls {
			testutil.TestingAssertEqual(t, len(poll.Answers), 2)
			err = NewAnswer(poll.ID, poll.Answers[0].ID, u.Email)
			testutil.TestingAssertEqual(t, err, nil)
			err = NewAnswer(poll.ID, poll.Answers[0].ID, u.Email)

			testutil.TestingAssertEqual(t, strings.Contains(err.Error(), "duplicate key value violates unique constraint"), true)
		}
	}

	err = DeleteUser("gollariel@gmail.com")
	testutil.TestingAssertEqual(t, err, nil)
}

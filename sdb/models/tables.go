package models

// All kind of tables
const (
	TableAnswers        = "answers"
	TableStreams        = "streams"
	TablePolls          = "polls"
	TablePollsToStreams = "polls_to_streams"
	TableUsers          = "users"
)

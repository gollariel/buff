package models

import (
	"time"

	"bitbucket.org/gollariel/buff/sdb"
	"bitbucket.org/gollariel/buff/utils"
	"github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx/types"
)

// Stream describe stream
type Stream struct {
	ID        int            `db:"id"`
	Title     string         `db:"title"`
	CreatedAt time.Time      `db:"created_at"`
	UpdatedAt time.Time      `db:"updated_at"`
	Polls     types.JSONText `db:"polls"`
}

// GetStreams return paginated streams
func GetStreams(limit, offset uint64) ([]Stream, error) {
	sql, _, err := squirrel.Select("s.id, s.title, s.created_at, s.updated_at, json_agg(p) as polls").
		From(utils.Concat(TableStreams, " s")).
		LeftJoin(utils.Concat(TablePollsToStreams, " pts on s.id = pts.stream_id")).
		Join(utils.Concat(TablePolls, " p on p.id = pts.poll_id")).
		GroupBy("s.id").
		Limit(offset).Offset(limit).ToSql()
	if err != nil {
		return nil, err
	}

	c, err := sdb.Default()
	if err != nil {
		return nil, err
	}

	var streams []Stream
	err = c.DB.Select(&streams, sql)
	if err != nil {
		return nil, err
	}
	return streams, nil
}

// CountStreams return count of streams
func CountStreams() (uint64, error) {
	var count uint64
	sql, args, err := squirrel.Select("COUNT(*)").
		From(utils.Concat(TableStreams, " s")).
		LeftJoin(utils.Concat(TablePollsToStreams, " pts on s.id = pts.stream_id")).
		Join(utils.Concat(TablePolls, " p on p.id = pts.poll_id")).
		GroupBy("s.id").ToSql()
	if err != nil {
		return count, err
	}

	c, err := sdb.Default()
	if err != nil {
		return count, err
	}

	query := c.DB.Rebind(sql)
	row := c.DB.QueryRow(query, args...)
	err = row.Scan(&count)
	if err != nil {
		return count, err
	}

	return count, nil
}

// TruncateAnswers truncate answers table
func TruncateAnswers() error {
	c, err := sdb.Default()
	if err != nil {
		return err
	}

	_, err = c.DB.Exec("TRUNCATE $1 CASCADE;", TableAnswers)
	if err != nil {
		return err
	}
	return nil
}

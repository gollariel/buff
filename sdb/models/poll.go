package models

// Poll describe single poll
type Poll struct {
	ID       int      `db:"id"`
	Question string   `db:"question"`
	Answers  []Answer `db:"answers"`
	StreamID int      `db:"stream_id"`
	Title    string   `db:"title"`
}

// Answer describe single answer
type Answer struct {
	ID      int    `json:"id"`
	Text    string `json:"text"`
	Correct bool   `json:"correct"`
}

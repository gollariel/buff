package models

import "errors"

// All kind of errors for models
var (
	ErrNotFoundUser = errors.New("error not found user")
)

# github.com/BurntSushi/toml v0.3.1
github.com/BurntSushi/toml
# github.com/Masterminds/squirrel v1.2.0
## explicit
github.com/Masterminds/squirrel
# github.com/davecgh/go-spew v1.1.1
github.com/davecgh/go-spew/spew
# github.com/dgrijalva/jwt-go v3.2.0+incompatible
## explicit
github.com/dgrijalva/jwt-go
# github.com/go-playground/locales v0.13.0
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.17.0
github.com/go-playground/universal-translator
# github.com/go-playground/validator/v10 v10.2.0
## explicit
github.com/go-playground/validator/v10
# github.com/google/gops v0.3.7
## explicit
github.com/google/gops/agent
github.com/google/gops/internal
github.com/google/gops/signal
# github.com/jmoiron/sqlx v1.2.0
## explicit
github.com/jmoiron/sqlx
github.com/jmoiron/sqlx/reflectx
github.com/jmoiron/sqlx/types
# github.com/kardianos/osext v0.0.0-20170510131534-ae77be60afb1
github.com/kardianos/osext
# github.com/kelseyhightower/envconfig v1.4.0
## explicit
github.com/kelseyhightower/envconfig
# github.com/lann/builder v0.0.0-20180802200727-47ae307949d0
github.com/lann/builder
# github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0
github.com/lann/ps
# github.com/leodido/go-urn v1.2.0
github.com/leodido/go-urn
# github.com/lib/pq v1.3.0
## explicit
github.com/lib/pq
github.com/lib/pq/oid
github.com/lib/pq/scram
# github.com/mailru/easyjson v0.7.1
## explicit
github.com/mailru/easyjson
github.com/mailru/easyjson/buffer
github.com/mailru/easyjson/gen
github.com/mailru/easyjson/jlexer
github.com/mailru/easyjson/jwriter
# github.com/pkg/errors v0.9.1
## explicit
github.com/pkg/errors
# github.com/pmezard/go-difflib v1.0.0
github.com/pmezard/go-difflib/difflib
# github.com/satori/go.uuid v1.2.0
## explicit
github.com/satori/go.uuid
# github.com/stretchr/testify v1.5.1
## explicit
github.com/stretchr/testify/assert
# go.uber.org/atomic v1.5.0
go.uber.org/atomic
# go.uber.org/multierr v1.3.0
go.uber.org/multierr
# go.uber.org/tools v0.0.0-20190618225709-2cfd321de3ee
go.uber.org/tools/update-license
# go.uber.org/zap v1.14.0
## explicit
go.uber.org/zap
go.uber.org/zap/buffer
go.uber.org/zap/internal/bufferpool
go.uber.org/zap/internal/color
go.uber.org/zap/internal/exit
go.uber.org/zap/zapcore
# golang.org/x/crypto v0.0.0-20190510104115-cbcb75029529
## explicit
golang.org/x/crypto/bcrypt
golang.org/x/crypto/blowfish
# golang.org/x/lint v0.0.0-20190930215403-16217165b5de
golang.org/x/lint
golang.org/x/lint/golint
# golang.org/x/tools v0.0.0-20191029190741-b9c20aec41a5
golang.org/x/tools/go/analysis
golang.org/x/tools/go/analysis/passes/inspect
golang.org/x/tools/go/ast/astutil
golang.org/x/tools/go/ast/inspector
golang.org/x/tools/go/buildutil
golang.org/x/tools/go/gcexportdata
golang.org/x/tools/go/internal/gcimporter
golang.org/x/tools/go/internal/packagesdriver
golang.org/x/tools/go/packages
golang.org/x/tools/go/types/objectpath
golang.org/x/tools/go/types/typeutil
golang.org/x/tools/internal/fastwalk
golang.org/x/tools/internal/gopathwalk
golang.org/x/tools/internal/semver
golang.org/x/tools/internal/span
# google.golang.org/appengine v1.6.5
## explicit
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
# honnef.co/go/tools v0.0.1-2019.2.3
honnef.co/go/tools/arg
honnef.co/go/tools/cmd/staticcheck
honnef.co/go/tools/config
honnef.co/go/tools/deprecated
honnef.co/go/tools/facts
honnef.co/go/tools/functions
honnef.co/go/tools/go/types/typeutil
honnef.co/go/tools/internal/cache
honnef.co/go/tools/internal/passes/buildssa
honnef.co/go/tools/internal/renameio
honnef.co/go/tools/internal/sharedcheck
honnef.co/go/tools/lint
honnef.co/go/tools/lint/lintdsl
honnef.co/go/tools/lint/lintutil
honnef.co/go/tools/lint/lintutil/format
honnef.co/go/tools/loader
honnef.co/go/tools/printf
honnef.co/go/tools/simple
honnef.co/go/tools/ssa
honnef.co/go/tools/ssautil
honnef.co/go/tools/staticcheck
honnef.co/go/tools/staticcheck/vrp
honnef.co/go/tools/stylecheck
honnef.co/go/tools/unused
honnef.co/go/tools/version

package datatypes

import (
	"sort"
	"strings"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestNewDataSet(t *testing.T) {
	testcases := map[string]struct {
		keys             []interface{}
		shouldContain    []interface{}
		shouldnotContain []interface{}
	}{
		"empty": {
			keys:             []interface{}{},
			shouldContain:    []interface{}{},
			shouldnotContain: []interface{}{" ", "abc"},
		},
		"valid": {
			keys: []interface{}{
				"one",
				"two",
				"three",
			},
			shouldContain:    []interface{}{"one", "two", "three"},
			shouldnotContain: []interface{}{"four", "", " ", ","},
		},
	}

	for exampleName, example := range testcases {
		t.Run(exampleName, func(t *testing.T) {
			set := NewDataSet(example.keys...)

			if len(example.shouldContain) != len(set) {
				t.Fatal("Invalid set length")
			}

			for _, key := range example.shouldContain {
				if !set.Contains(key) {
					t.Fatalf("expected set: %v to contain: %s", set, key)
				}
			}

			for _, key := range example.shouldnotContain {
				if set.Contains(key) {
					t.Fatalf("expected set: %v to not contain: %s", set, key)
				}
			}

			res := set.ToSlice()
			sort.Slice(example.keys, func(i, j int) bool {
				return strings.Compare(example.keys[i].(string), example.keys[j].(string)) <= 0
			})
			sort.Slice(res, func(i, j int) bool {
				return strings.Compare(res[i].(string), res[j].(string)) <= 0
			})
			testutil.TestingAssertEqual(t, res, example.keys)
		})
	}
}

func TestNewDataSetStrings(t *testing.T) {
	testcases := map[string]struct {
		keys             []string
		shouldContain    []string
		shouldnotContain []string
	}{
		"empty": {
			keys:             []string{},
			shouldContain:    []string{},
			shouldnotContain: []string{" ", "abc"},
		},
		"valid": {
			keys: []string{
				"one",
				"two",
				"three",
			},
			shouldContain:    []string{"one", "two", "three"},
			shouldnotContain: []string{"four", "", " ", ","},
		},
	}

	for exampleName, example := range testcases {
		t.Run(exampleName, func(t *testing.T) {
			set := NewDataSetStrings(example.keys...)

			if len(example.shouldContain) != len(set) {
				t.Fatal("Invalid set length")
			}

			for _, key := range example.shouldContain {
				if !set.Contains(key) {
					t.Fatalf("expected set: %v to contain: %s", set, key)
				}
			}

			for _, key := range example.shouldnotContain {
				if set.Contains(key) {
					t.Fatalf("expected set: %v to not contain: %s", set, key)
				}
			}

			res := set.ToSlice()
			sort.Slice(example.keys, func(i, j int) bool {
				return strings.Compare(example.keys[i], example.keys[j]) <= 0
			})
			sort.Slice(res, func(i, j int) bool {
				return strings.Compare(res[i], res[j]) <= 0
			})
			testutil.TestingAssertEqual(t, res, example.keys)
		})
	}
}

func TestNewDataSetInts(t *testing.T) {
	testcases := map[string]struct {
		keys             []int
		shouldContain    []int
		shouldnotContain []int
	}{
		"empty": {
			keys:             []int{},
			shouldContain:    []int{},
			shouldnotContain: []int{0, 1},
		},
		"valid": {
			keys: []int{
				1,
				2,
				3,
			},
			shouldContain:    []int{2, 3, 1},
			shouldnotContain: []int{4, 0},
		},
	}

	for exampleName, example := range testcases {
		t.Run(exampleName, func(t *testing.T) {
			set := NewDataSetInts(example.keys...)

			if len(example.shouldContain) != len(set) {
				t.Fatal("Invalid set length")
			}

			for _, key := range example.shouldContain {
				if !set.Contains(key) {
					t.Fatalf("expected set: %v to contain: %d", set, key)
				}
			}

			for _, key := range example.shouldnotContain {
				if set.Contains(key) {
					t.Fatalf("expected set: %v to not contain: %d", set, key)
				}
			}

			res := set.ToSlice()
			sort.Slice(example.keys, func(i, j int) bool {
				return example.keys[i] <= example.keys[j]
			})
			sort.Slice(res, func(i, j int) bool {
				return res[i] <= res[j]
			})
			testutil.TestingAssertEqual(t, res, example.keys)
		})
	}
}

func TestNewUint32Set(t *testing.T) {
	set := NewMultiUint32Set()
	set.Add(14, 10)
	testutil.TestingAssertEqual(t, set.Contains(10, 14), true)
	testutil.TestingAssertEqual(t, set.Contains(14, 10), true)
	testutil.TestingAssertEqual(t, set.Contains(13, 10), false)
	testutil.TestingAssertEqual(t, set.Contains(13, 14), false)
}

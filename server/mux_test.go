package server

import (
	"fmt"
	"net/http"
	"testing"

	"bitbucket.org/gollariel/buff/utils/testutil"
)

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Call")
	fmt.Fprint(w, "Welcome!\n")
}

func hello2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Call2")
	fmt.Fprint(w, "Welcome2!\n")
}

func hello3(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Call3")
	fmt.Fprint(w, "Welcome3!\n")
}

func TestIndex(t *testing.T) {
	i := Root()
	i.Add("/address/:name/:id/:type", NewRoute("", http.HandlerFunc(hello)))
	i.Add("/address/:name/:id", NewRoute(http.MethodGet, http.HandlerFunc(hello2)))
	i.Add("/s/*", NewRoute(http.MethodPost, http.HandlerFunc(hello3)))

	r, values, e := i.Get(MethodAny, "/address/max/123/request")
	testutil.TestingAssertEqual(t, r != nil, true)
	testutil.TestingAssertEqual(t, e, true)
	testutil.TestingAssertEqual(t, len(values), 3)
	testutil.TestingAssertEqual(t, values["id"], "123")
	testutil.TestingAssertEqual(t, values["name"], "max")
	testutil.TestingAssertEqual(t, values["type"], "request")
	r, values, e = i.Get(http.MethodGet, "/address/max/123")
	testutil.TestingAssertEqual(t, r != nil, true)
	testutil.TestingAssertEqual(t, e, true)
	testutil.TestingAssertEqual(t, len(values), 2)
	testutil.TestingAssertEqual(t, values["id"], "123")
	testutil.TestingAssertEqual(t, values["name"], "max")
	r, _, e = i.Get(http.MethodGet, "/address/max/123/1/2")
	testutil.TestingAssertEqual(t, r != nil, false)
	testutil.TestingAssertEqual(t, e, false)
	r, _, e = i.Get(MethodAny, "/address/max")
	testutil.TestingAssertEqual(t, r != nil, false)
	testutil.TestingAssertEqual(t, e, false)
	r, values, e = i.Get(http.MethodPost, "/s/test.png")
	testutil.TestingAssertEqual(t, r != nil, true)
	testutil.TestingAssertEqual(t, e, true)
	testutil.TestingAssertEqual(t, len(values), 1)
	testutil.TestingAssertEqual(t, values["*"], "test.png")
	r, values, e = i.Get(http.MethodPost, "/s/arxhive/test.png")
	testutil.TestingAssertEqual(t, r != nil, true)
	testutil.TestingAssertEqual(t, e, true)
	testutil.TestingAssertEqual(t, len(values), 1)
	testutil.TestingAssertEqual(t, values["*"], "arxhive,test.png")

	r, _, e = i.Get(http.MethodGet, "/")
	testutil.TestingAssertEqual(t, r != nil, false)
	testutil.TestingAssertEqual(t, e, false)
	i.Add("/", NewRoute(http.MethodGet, http.HandlerFunc(hello)))
	r, values, e = i.Get(http.MethodGet, "/")
	testutil.TestingAssertEqual(t, r != nil, true)
	testutil.TestingAssertEqual(t, e, true)
	testutil.TestingAssertEqual(t, len(values), 0)
	r, _, e = i.Get(MethodAny, "/a")
	testutil.TestingAssertEqual(t, r != nil, false)
	testutil.TestingAssertEqual(t, e, false)
}

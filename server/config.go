package server

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

// EnvPrefix environment prefix for redis config
const EnvPrefix = "SERVER"

// ConnectionConfig contains listen url for the server and additional options
type ConnectionConfig struct {
	Addr         string        `required:"true"`
	ReadTimeout  time.Duration `default:"60s" split_words:"true"`
	WriteTimeout time.Duration `default:"60s" split_words:"true"`
}

// NewConnectionConfig return new server connection config
func NewConnectionConfig(addr string, readTimeout, writeTimeout time.Duration) *ConnectionConfig {
	return &ConnectionConfig{
		Addr:         addr,
		ReadTimeout:  readTimeout,
		WriteTimeout: writeTimeout,
	}
}

// GetConnectionConfigFromEnv return server configs bases on environment variables
func GetConnectionConfigFromEnv() (*ConnectionConfig, error) {
	c := new(ConnectionConfig)
	err := envconfig.Process(EnvPrefix, c)
	return c, err
}

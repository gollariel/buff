package server

import (
	"net/http"
)

// ListenAndServe start fasthttp listener
func ListenAndServe(handler http.Handler) error {
	c, err := GetConnectionConfigFromEnv()
	if err != nil {
		return err
	}
	httpServer := http.Server{
		Addr:         c.Addr,
		Handler:      handler,
		ReadTimeout:  c.ReadTimeout,
		WriteTimeout: c.WriteTimeout,
	}

	return httpServer.ListenAndServe()
}

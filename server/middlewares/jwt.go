package middlewares

import (
	"context"
	"net/http"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
)

// Contains default values
const (
	DefaultService = "default"
	DefaultScope   = "default"
)

// JWTAuthenticator jwt authenticator
func JWTAuthenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenHeader := r.Header.Get("Authorization")
		scope, ok := r.Context().Value(jwt.CtxScope).(string)
		if !ok {
			fastlog.Error("Error getting scope")
			http.Error(w, "Error getting scope", http.StatusForbidden)
			return
		}
		service, ok := r.Context().Value(jwt.CtxService).(string)
		if !ok {
			fastlog.Error("Error getting service")
			http.Error(w, "Error getting service", http.StatusForbidden)
			return
		}
		if len(tokenHeader) <= 7 {
			fastlog.Error("Error length of token header", "tokenHeader", tokenHeader)
			http.Error(w, "Error length of token header", http.StatusForbidden)
			return
		}
		claim, err := jwt.Validate(tokenHeader[7:], service, scope)
		if err != nil {
			fastlog.Error("Error validating jwt", "err", err)
			http.Error(w, "Error validating jwt", http.StatusForbidden)
			return
		}
		r = r.WithContext(context.WithValue(r.Context(), jwt.CtxEmail, claim.Issuer))
		next.ServeHTTP(w, r)
	})
}

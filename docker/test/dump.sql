-- Adminer 4.7.6 PostgreSQL dump

DROP TABLE IF EXISTS "users";
CREATE TABLE "public"."users" (
    "email" character varying(255) NOT NULL,
    "password" text,
    "first_name" text NOT NULL,
    "last_name" text NOT NULL,
    "birthday" timestamp NOT NULL,
    "scopes" text[] NOT NULL,
    CONSTRAINT "users_pkey" PRIMARY KEY ("email")
) WITH (oids = false);

INSERT INTO "users" ("email", "password", "first_name", "last_name", "birthday", "scopes") VALUES
('test@test.ua',	'$2a$08$UMf0h.AYwm7ZldHa.nuZnuwErN2pBItzahuak.Y1nyTpGxCFFOY9W',	'123',	'321',	'2020-03-15 13:17:07.693356',	'{stream,user}');

DROP TABLE IF EXISTS "polls";
CREATE TABLE "public"."polls" (
    "id" serial,
    "question" text NOT NULL,
    "answers" jsonb NOT NULL,
    CONSTRAINT "polls_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
INSERT INTO "polls" ("id", "question", "answers") VALUES
(1,	'Is it true?',	'[{"id": 1, "text": "yes", "correct": true}, {"id": 2, "text": "no", "correct": false}]'),
(2,	'Is it false?',	'[{"id": 1, "text": "yes", "correct": false}, {"id": 2, "text": "no", "correct": true}]');

DROP TABLE IF EXISTS "streams";
CREATE TABLE "public"."streams" (
    "id" serial,
    "title" text NOT NULL,
    "created_at" timestamp,
    "updated_at" timestamp,
    CONSTRAINT "streams_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
INSERT INTO "streams" ("id", "title", "created_at", "updated_at") VALUES
(1,	'example1',	'2020-03-13 18:29:55.132484',	'2020-03-13 18:29:55.132484'),
(2,	'example2',	'2020-03-13 18:30:06.193135',	'2020-03-13 18:30:06.193135');

DROP TABLE IF EXISTS "answers";
CREATE TABLE "public"."answers" (
    "id" serial,
    "email" character varying(255) NOT NULL,
    "poll_id" integer NOT NULL,
    "answer_id" integer NOT NULL,
    CONSTRAINT "answers_poll_id_email" PRIMARY KEY ("poll_id", "email"),
    CONSTRAINT "answers_email_fkey" FOREIGN KEY (email) REFERENCES users(email) ON DELETE CASCADE NOT DEFERRABLE,
    CONSTRAINT "answers_poll_id_fkey" FOREIGN KEY (poll_id) REFERENCES polls(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);

DROP TABLE IF EXISTS "polls_to_streams";
CREATE TABLE "public"."polls_to_streams" (
    "poll_id" integer,
    "stream_id" integer,
    CONSTRAINT "polls_to_streams_poll_id_fkey" FOREIGN KEY (poll_id) REFERENCES polls(id) ON DELETE CASCADE NOT DEFERRABLE,
    CONSTRAINT "polls_to_streams_stream_id_fkey" FOREIGN KEY (stream_id) REFERENCES streams(id) ON DELETE CASCADE NOT DEFERRABLE
) WITH (oids = false);
INSERT INTO "polls_to_streams" ("poll_id", "stream_id") VALUES
(1,	1),
(2,	1);

package jwt

import (
	"bitbucket.org/gollariel/buff/datatypes"
	"github.com/dgrijalva/jwt-go"
)

// Claim custom claim
type Claim struct {
	jwt.StandardClaims
	Scopes datatypes.DataSetStrings `json:"scp,omitempty"`
}

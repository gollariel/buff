package jwt

import (
	"testing"
	"time"

	"bitbucket.org/gollariel/buff/datatypes"
	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestJWT(t *testing.T) {
	user := "jackdaniels"
	service := "base"
	scopes := datatypes.NewDataSetStrings("read")
	rawToken, err := Create(user, service, scopes, time.Time{})
	testutil.TestingAssertEqual(t, err, nil)
	_, err = Validate(rawToken, "base", "write")
	testutil.TestingAssertEqual(t, err, ErrUserNotInScope)
	claim, err := Validate(rawToken, "base", "read")
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, claim.Issuer, user)
}

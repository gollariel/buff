package jwt

import "errors"

// All kind of errors for jwt
var (
	ErrUnexpectedSigningMethod = errors.New("unexpected signing method")
	ErrUserNotInScope          = errors.New("not in scope")
	ErrUserWrongService        = errors.New("wrong service")
	ErrInvalidToken            = errors.New("invalid token")
)

package jwt

import (
	"os"
	"time"

	"bitbucket.org/gollariel/buff/datatypes"
	"bitbucket.org/gollariel/buff/utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

// EnvJWTSigningKey contains name of environment variable for jwt signing key
const EnvJWTSigningKey = "JWT_SIGNING_KEY"

// Context keys
const (
	CtxEmail   utils.ContextKey = "email"
	CtxScope   utils.ContextKey = "scope"
	CtxService utils.ContextKey = "service"
)

// GetSigningKey return signed key
func GetSigningKey() string {
	key := os.Getenv(EnvJWTSigningKey)
	if key == "" {
		key = "ssk_uper_ecret_ey"
	}
	return key
}

// Create create jwt token
func Create(user string, service string, scopes datatypes.DataSetStrings, expiration time.Time) (string, error) {
	c := jwt.StandardClaims{
		Issuer:   user,
		Audience: service,
	}
	if !expiration.IsZero() {
		c.ExpiresAt = expiration.Unix()
	}

	key := GetSigningKey()
	claims := Claim{
		StandardClaims: c,
		Scopes:         scopes,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(key))
}

// Validate validate jwt token
func Validate(tokenRaw string, service string, scope string) (Claim, error) {
	key := GetSigningKey()
	var claim Claim
	token, err := jwt.ParseWithClaims(tokenRaw, &claim, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.Wrapf(ErrUnexpectedSigningMethod, "alg: %s", token.Header["alg"])
		}
		return []byte(key), nil
	})
	if err != nil {
		return claim, err
	}

	if !token.Valid {
		return claim, ErrInvalidToken
	}

	if claim.Audience != service {
		return claim, ErrUserWrongService
	}

	if !claim.Scopes.Contains(scope) {
		return claim, ErrUserNotInScope
	}

	return claim, nil
}

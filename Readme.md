## Poller

Simple service to collect users' answers, related to different streams.

### Run

For start service, just execute: `docker-compose up --build`
For start service in dev mode, execute: `docker-compose -f ./docker-compose.dev.yml  up --build`

### API

We support next API endpoints

#### Authorization 

* *Method*: POST
* *Path*: /auth
* *Auth*: false
* *Params*: 
    - email - user email
    - password - user password
* *Response*: In header JWT Bearer token

#### Create user

* *Method*: POST
* *Path*: /user
* *Auth*: false
* *Params*: 
    - email - user email
    - password - user password (min 6 symbols)
    - first_name - user first name
    - last_name - user last name
    - birthday - date of birth (format example: 1992-01-23T00:00:00Z)

#### Update user

* *Method*: PUT
* *Path*: /user
* *Auth*: true (wait for JWT Bearer token in header)
* *Params*: 
    - first_name - user first name
    - last_name - user last name
    - birthday - date of birth (format example: 1992-01-23T00:00:00Z)

#### Get user

* *Method*: GET
* *Path*: /user
* *Auth*: true (wait for JWT Bearer token in header)
* *Response*: User object

#### Delete user

* *Method*: DELETE
* *Path*: /user
* *Auth*: true (wait for JWT Bearer token in header)

#### Get all streams

* *Method*: GET
* *Path*: /stream
* *Auth*: true (wait for JWT Bearer token in header)
* *Params*: 
    - page - current page id

#### Save new answer

* *Method*: POST
* *Path*: /stream/:id
* *Auth*: true (wait for JWT Bearer token in header)
* *Params*: 
    - poll_id - ID of poll
    - answer_id - ID of answer

### Testing

To check all tests with relations, execute: `make test-docker`.

For check linter execute: `make lint`.
To check all tests execute: `make test`. (postgres sql should be run, and environment variables should be present)
To build a new binary: `make build`.

### Debug

Service includes GOPS. This is the helper for debug performance issues. By default, GOPS runs on 6060 port.

### Configuration

All configurations are able via environment variables.
For example: 

- SERVER_ADDR - addr for listening
- SQL_DRIVER_NAME - database driver
- SQL_SOURCE_NAME - URI for connecting to DB
- DEBUG_LEVEL - what kind of message shows
- LOG_NAME - a special name for add to each log messages (useful for use in different services)

**Warning! We should improve API tests. Current implementation do not cover all corner cases**
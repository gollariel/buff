package poll

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/gollariel/buff/datatypes"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/sdb/models"
	"bitbucket.org/gollariel/buff/server"
	"bitbucket.org/gollariel/buff/server/middlewares"
	"bitbucket.org/gollariel/buff/utils"
	"bitbucket.org/gollariel/buff/utils/testutil"
)

func TestUser(t *testing.T) {
	sm := server.NewMiddleware(middlewares.Recover, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxService, models.BaseServiceName))
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxScope, models.ScopeStream))
			next.ServeHTTP(w, r)
		})
	}, middlewares.JWTAuthenticator)

	token, err := jwt.Create("test@test.ua", models.BaseServiceName, datatypes.NewDataSetStrings(models.ScopeStream), time.Now().Add(time.Hour))
	testutil.TestingAssertEqual(t, err, nil)
	reqGet, err := http.NewRequest(http.MethodGet, "/stream", nil)
	testutil.TestingAssertEqual(t, err, nil)
	reqGet.Header.Add("Authorization", utils.Concat("Bearer ", token))
	rr := httptest.NewRecorder()
	sm.Then(http.HandlerFunc((&Handler{}).GetStreams)).ServeHTTP(rr, reqGet)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusOK)
	var requestData struct {
		Data struct {
			Streams []models.Stream `json:"streams"`
		} `json:"data"`
	}
	err = json.Unmarshal(rr.Body.Bytes(), &requestData)
	testutil.TestingAssertEqual(t, err, nil)
	testutil.TestingAssertEqual(t, len(requestData.Data.Streams), 1)
	testutil.TestingAssertEqual(t, requestData.Data.Streams[0].Title, "example1")

	reqDelete, err := http.NewRequest(http.MethodPost, "/stream/1", bytes.NewBufferString(`{
	"poll_id": 1,
	"answer_id": 1
	}`))
	testutil.TestingAssertEqual(t, err, nil)
	reqDelete.Header.Add("Authorization", utils.Concat("Bearer ", token))
	rr = httptest.NewRecorder()
	sm.Then(http.HandlerFunc((&Handler{}).Answer)).ServeHTTP(rr, reqDelete)
	testutil.TestingAssertEqual(t, rr.Code, http.StatusAccepted)
}

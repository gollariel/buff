package poll

import (
	"math"
	"net/http"
	"strconv"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/requests"
	"bitbucket.org/gollariel/buff/sdb/models"
	"bitbucket.org/gollariel/buff/utils"
)

const (
	defaultOffset uint64 = 10
)

// Handler describe handler
type Handler struct {
}

// NewHandler return new handler
func NewHandler() *Handler {
	return &Handler{}
}

// GetStreams get paginated streams
func (h *Handler) GetStreams(w http.ResponseWriter, r *http.Request) {
	page, err := strconv.ParseUint(r.URL.Query().Get("page"), 10, 64)
	if err != nil {
		page = 1
	}
	streams, err := models.GetStreams((page-1)*defaultOffset, defaultOffset)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	count, err := models.CountStreams()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp := utils.NewResponseJSON()
	resp.Add("streams", streams)
	resp.Add("page", page)
	resp.Add("perPage", defaultOffset)
	resp.Add("pages", int(math.Ceil(float64(count)/float64(defaultOffset))))
	resp.SetStatus(http.StatusOK)
	resp.Send(w)
}

// Answer save user answer
func (h *Handler) Answer(w http.ResponseWriter, r *http.Request) {
	var answerRequest requests.AnswerRequest
	err := requests.DecodeRequest(r, &answerRequest)
	if err != nil {
		fastlog.Error("Error decoding request", "err", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	email := r.Context().Value(jwt.CtxEmail).(string)
	err = models.NewAnswer(answerRequest.PollID, answerRequest.AnswerID, email)
	if err != nil {
		fastlog.Error("Error adding answer", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := utils.NewResponseJSON()
	resp.SetStatus(http.StatusAccepted)
	resp.Send(w)
}

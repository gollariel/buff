package poll

import "errors"

// All kind of errors
var (
	ErrInvalidEmail    = errors.New("error invalid email")
	ErrInvalidPassword = errors.New("error invalid password")
)

package main

import (
	"context"
	"net/http"

	"bitbucket.org/gollariel/buff/fastlog"
	"bitbucket.org/gollariel/buff/jwt"
	"bitbucket.org/gollariel/buff/poll"
	"bitbucket.org/gollariel/buff/sdb/models"
	"bitbucket.org/gollariel/buff/server"
	"bitbucket.org/gollariel/buff/server/middlewares"
	"bitbucket.org/gollariel/buff/user"
	"bitbucket.org/gollariel/buff/utils"
)

func main() {
	if err := utils.RunGopsInstance(); err != nil {
		fastlog.Error("Failed to run gops agent", "error", err)
	}

	mr := server.NewMiddleware(middlewares.Recover)
	sm := server.NewMiddleware(middlewares.Recover, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxService, models.BaseServiceName))
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxScope, models.ScopeStream))
			next.ServeHTTP(w, r)
		})
	}, middlewares.JWTAuthenticator)
	um := server.NewMiddleware(middlewares.Recover, func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxService, models.BaseServiceName))
			r = r.WithContext(context.WithValue(r.Context(), jwt.CtxScope, models.ScopeUser))
			next.ServeHTTP(w, r)
		})
	}, middlewares.JWTAuthenticator)

	userHandler := user.NewHandler()
	pollHandler := poll.NewHandler()
	index := server.Root()
	index.Add("/auth", server.NewRoute(http.MethodPost, mr.Then(http.HandlerFunc(userHandler.Auth))))
	index.Add("/user",
		server.NewRoute(http.MethodPost, mr.Then(http.HandlerFunc(userHandler.Create))),
		server.NewRoute(http.MethodPut, um.Then(http.HandlerFunc(userHandler.Update))),
		server.NewRoute(http.MethodGet, um.Then(http.HandlerFunc(userHandler.Get))),
		server.NewRoute(http.MethodDelete, um.Then(http.HandlerFunc(userHandler.Delete))),
	)
	index.Add("/stream", server.NewRoute(http.MethodGet, sm.Then(http.HandlerFunc(pollHandler.GetStreams))))
	index.Add("/stream/:id",
		server.NewRoute(http.MethodPost, sm.Then(http.HandlerFunc(pollHandler.Answer))),
	)

	if err := server.ListenAndServe(index.Handler(nil)); err != nil {
		fastlog.Fatal("Failed start fast http server", "err", err)
	}
}

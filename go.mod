module bitbucket.org/gollariel/buff

go 1.14

require (
	github.com/Masterminds/squirrel v1.2.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.2.0
	github.com/google/gops v0.3.7
	github.com/jmoiron/sqlx v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.3.0
	github.com/mailru/easyjson v0.7.1
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.14.0
	golang.org/x/crypto v0.0.0-20190510104115-cbcb75029529
	google.golang.org/appengine v1.6.5 // indirect
)

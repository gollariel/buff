package requests

//go:generate easyjson -all ./auth.go
import (
	"bitbucket.org/gollariel/buff/utils"
	"github.com/go-playground/validator/v10"
	_ "github.com/mailru/easyjson/gen" // nolint
)

// AuthRequest auth request
type AuthRequest struct {
	Password utils.Password `json:"password" validate:"required,min=5"`
	Email    string         `json:"email" validate:"required,email,min=3,max=200"`
}

// Validate check is request valid
func (a *AuthRequest) Validate() error {
	return validator.New().Struct(a)
}

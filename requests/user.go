package requests

//go:generate easyjson -all ./user.go
import (
	"time"

	"bitbucket.org/gollariel/buff/utils"
	"github.com/go-playground/validator/v10"
	_ "github.com/mailru/easyjson/gen" // nolint
)

// UserRequest user request
type UserRequest struct {
	Password  utils.Password `json:"password" validate:"required,min=5"`
	Email     string         `json:"email" validate:"required,email,min=3,max=200"`
	FirstName string         `json:"first_name" validate:"required,min=3,max=200"`
	LastName  string         `json:"last_name" validate:"required,min=3,max=200"`
	Birthday  time.Time      `json:"birthday" validate:"required"`
}

// Validate check is request valid
func (c *UserRequest) Validate() error {
	return validator.New().Struct(c)
}

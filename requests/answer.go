package requests

//go:generate easyjson -all ./answer.go
import (
	"github.com/go-playground/validator/v10"
	_ "github.com/mailru/easyjson/gen" // nolint
)

// AnswerRequest answer request
type AnswerRequest struct {
	PollID   int `json:"poll_id" validate:"required"`
	AnswerID int `json:"answer_id" validate:"required"`
}

// Validate check is request valid
func (a *AnswerRequest) Validate() error {
	return validator.New().Struct(a)
}

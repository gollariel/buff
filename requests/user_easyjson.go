// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package requests

import (
	utils "bitbucket.org/gollariel/buff/utils"
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjson9e1087fdDecodeBitbucketOrgGollarielBuffRequests(in *jlexer.Lexer, out *UserRequest) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeString()
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "password":
			out.Password = utils.Password(in.String())
		case "email":
			out.Email = string(in.String())
		case "first_name":
			out.FirstName = string(in.String())
		case "last_name":
			out.LastName = string(in.String())
		case "birthday":
			if data := in.Raw(); in.Ok() {
				in.AddError((out.Birthday).UnmarshalJSON(data))
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjson9e1087fdEncodeBitbucketOrgGollarielBuffRequests(out *jwriter.Writer, in UserRequest) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"password\":"
		out.RawString(prefix[1:])
		out.Raw((in.Password).MarshalJSON())
	}
	{
		const prefix string = ",\"email\":"
		out.RawString(prefix)
		out.String(string(in.Email))
	}
	{
		const prefix string = ",\"first_name\":"
		out.RawString(prefix)
		out.String(string(in.FirstName))
	}
	{
		const prefix string = ",\"last_name\":"
		out.RawString(prefix)
		out.String(string(in.LastName))
	}
	{
		const prefix string = ",\"birthday\":"
		out.RawString(prefix)
		out.Raw((in.Birthday).MarshalJSON())
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v UserRequest) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjson9e1087fdEncodeBitbucketOrgGollarielBuffRequests(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v UserRequest) MarshalEasyJSON(w *jwriter.Writer) {
	easyjson9e1087fdEncodeBitbucketOrgGollarielBuffRequests(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *UserRequest) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjson9e1087fdDecodeBitbucketOrgGollarielBuffRequests(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *UserRequest) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjson9e1087fdDecodeBitbucketOrgGollarielBuffRequests(l, v)
}

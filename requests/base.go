package requests

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"
)

// Validator describe validator interface
type Validator interface {
	Validate() error
}

// DecodeRequest decode request into given type
func DecodeRequest(r *http.Request, d Validator) error {
	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		return err
	}

	err = r.Body.Close()
	if err != nil {
		return err
	}

	return d.Validate()
}

// DecodeMapRequest decode request into given type
func DecodeMapRequest(r *http.Request, rules map[string]string) (map[string]interface{}, error) {
	var d map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		return nil, err
	}

	err = r.Body.Close()
	if err != nil {
		return nil, err
	}

	val := validator.New()
	result := map[string]interface{}{}

	for k, r := range rules {
		if v, ok := d[k]; ok {
			if r != "" {
				err = val.Var(v, r)
				if err != nil {
					return nil, err
				}
			}
			result[k] = v
		}
	}
	return result, nil
}

VERSION?=$(shell git describe --tags)

build:
	CGO_ENABLED=0 env GOOS=linux GOARCH=amd64 go build -ldflags "-X main.version=${VERSION}" -o bin/server ./cmd/poll-server

test:
	go test -timeout 10s -race -cover -p 1 ./...

test-docker:
	docker-compose -f docker/test/docker-compose.yml up --build --abort-on-container-exit
	docker-compose -f docker/test/docker-compose.yml down

format:
	gofmt -s -w **/*.go

init:
	chmod -R +x .githooks/
	mkdir -p .git/hooks/
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

lint:
	golangci-lint run --exclude-use-default=false --skip-dirs=vendor --disable-all --enable=goimports --enable=gosimple --enable=typecheck --enable=unused --enable=golint --enable=deadcode --enable=structcheck --enable=varcheck --enable=errcheck --enable=ineffassign --enable=govet --enable=staticcheck --enable=gofmt --deadline=3m ./...

.PHONY: test test-cover format init lint